# kullback-leibler-divergence-examples

Examples demonstrating Kullback-Leibler divergence. See [the notebook](./kullback-leibler-divergence.ipynb).

## Credits

Icons made by [Payungkead](https://www.flaticon.com/authors/payungkead).